fun	subUpdateFacts(infos: Infos, result: String, line: String, queryEnd: String): Infos {
	if (result == "undefined") return infos;
	if (result == "true") {
		"[A-Z]".toRegex().findAll(queryEnd).forEach{
			if (!infos.facts.areTrue.contains(it.groupValues[0]) && !infos.facts.areFalse.contains(it.groupValues[0])) {
				infos.index = -1;
				infos.facts.areTrue.add(it.groupValues[0]);
			}
		}
	}
	else if (result == "false") {
		"[A-Z]".toRegex().findAll(queryEnd).forEach{
			if (!infos.facts.areTrue.contains(it.groupValues[0]) && !infos.facts.areFalse.contains(it.groupValues[0])) {
				infos.index = -1;
				infos.facts.areFalse.add(it.groupValues[0]);
			}
		}
	}
	val definedFacts: MutableList<String> = mutableListOf<String>();
	definedFacts.addAll(infos.facts.areTrue);
	definedFacts.addAll(infos.facts.areFalse);
	if (definedFacts.size != definedFacts.distinct().size) {
		println("uncorrect line : ${line}\nfact has different results");
		infos.error = true;
	}
	return infos;
}

fun subResolveRules(infos: Infos): Infos {
    infos.index = -1;
	while (++infos.index < infos.rules.size) {
		val rule: Rule = infos.rules[infos.index];
		if (rule.result != "undefined") continue ;
		val (queryStart: String, queryEnd: String) = rule.query.replace("\\s", "").split("=>");
		subUpdateFacts(infos, resolveRule(queryStart, infos.facts), rule.query, queryEnd);
		if (infos.error) return infos;
	}
	return infos;
}
