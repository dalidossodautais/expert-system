class Rule(val query: String, var result: String = "undefined");
class Facts(
	val areTrue: MutableList<String> = mutableListOf<String>(),
	val areFalse: MutableList<String> = mutableListOf<String>(),
	val areSought: MutableList<String> = mutableListOf<String>()
);
class Infos(
	val facts: Facts = Facts(),
	val rules: MutableList<Rule> = mutableListOf<Rule>(),
	var index: Int = -1,
	var unrecoverable: String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
	var error: Boolean = false
);

fun	main(args: Array<String>): Unit {
	if (args.size == 0) return println("enter filename");
	var index: Int = -1;
	while (++index < args.size) {
		println(args[index]);
		var infos: Infos = Infos();
		infos = parseFile(infos, args[index]);
		if (infos.error) continue;
		infos = resolveRules(infos);
		if (infos.error) continue;
		infos.unrecoverable.forEach{ infos.facts.areFalse.add("${it}") }
		infos = subResolveRules(infos);
		if (infos.error) continue;
		infos.facts.areSought.forEach { fact ->
			println("${fact} is ${infos.facts.areTrue.contains(fact)}");
		}
	}
}