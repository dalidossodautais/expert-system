fun replaceFactsByValues(query: String, facts: Facts): String {
	var thisQuery: String = query;
	while ("[A-Z]".toRegex().containsMatchIn(thisQuery)) {
		val fact: String = "[A-Z]".toRegex().find(thisQuery)!!.value;
		thisQuery = thisQuery.replace(fact,
			if (facts.areTrue.contains(fact)) "true"
			else if (facts.areFalse.contains(fact)) "false"
			else "undefined"
		);
	}
	thisQuery = "!undefined".toRegex().replace(thisQuery, "undefined");
	thisQuery = "!false".toRegex().replace(thisQuery, "true");
	thisQuery = "!true".toRegex().replace(thisQuery, "false");
	thisQuery = "([a-z]+\\+)*undefined(\\+[a-z]+)*".toRegex().replace(thisQuery, "undefined");
	thisQuery = "([a-z]+\\+)*false(\\+[a-z]+)*".toRegex().replace(thisQuery, "false");
	thisQuery = "([a-z]+\\+)*true(\\+[a-z]+)*".toRegex().replace(thisQuery, "true");
	thisQuery = "([a-z]+\\|)*true(\\|[a-z]+)*".toRegex().replace(thisQuery, "true");
	thisQuery = "([a-z]+\\|)*undefined(\\|[a-z]+)*".toRegex().replace(thisQuery, "undefined");
	thisQuery = "([a-z]+\\|)*false(\\|[a-z]+)*".toRegex().replace(thisQuery, "false");
	thisQuery = "([a-z]+\\^)*undefined(\\^[a-z]+)*".toRegex().replace(thisQuery, "undefined");
	while ("\\^".toRegex().containsMatchIn(thisQuery)) {
		thisQuery = "(true\\^false|false\\^true)".toRegex().replace(thisQuery, "true");
		thisQuery = "(true\\^true|false\\^false)".toRegex().replace(thisQuery, "false");
	}
	return thisQuery;
}

fun	resolveRule(query: String, facts: Facts): String {
	var thisQuery: String = query;
	while ("\\(".toRegex().containsMatchIn(thisQuery)) {
		val midQuery: String = "\\([^\\(]+\\)".toRegex().find(thisQuery)!!.value;
		val subQuery: String = resolveRule(midQuery.slice(1..midQuery.length - 2), facts);
		thisQuery = thisQuery.replaceFirst("\\([^\\(]+\\)".toRegex(), subQuery);
	}
	return replaceFactsByValues(thisQuery, facts);
}

fun	updateFacts(infos: Infos, result: String, line: String, queryEnd: String): Infos {
	if (result == "undefined") return infos;
	if (result == "true") {
		"[A-Z]".toRegex().findAll(queryEnd).forEach{
			if (!infos.facts.areTrue.contains(it.groupValues[0])) {
				infos.index = -1;
				infos.facts.areTrue.add(it.groupValues[0]);
			}
		}
	}
	else if (result == "false") {
		"[A-Z]".toRegex().findAll(queryEnd).forEach{
			if (!infos.facts.areFalse.contains(it.groupValues[0])) {
				infos.index = -1;
				infos.facts.areFalse.add(it.groupValues[0]);
			}
		}
	}
	val definedFacts: MutableList<String> = mutableListOf<String>();
	definedFacts.addAll(infos.facts.areTrue);
	definedFacts.addAll(infos.facts.areFalse);
	if (definedFacts.size != definedFacts.distinct().size) {
		println("uncorrect line : ${line}\nfact has different results");
		infos.error = true;
	}
	return infos;
}

fun	resolveRules(infos: Infos): Infos {
	while (++infos.index < infos.rules.size) {
		val rule: Rule = infos.rules[infos.index];
		if (rule.result != "undefined") continue ;
		val (queryStart: String, queryEnd: String) = rule.query.replace("\\s", "").split("=>");
		updateFacts(infos, resolveRule(queryStart, infos.facts), rule.query, queryEnd);
		if (infos.error) return infos;
	}
	return infos;
}
