import java.io.File

fun checkQuery(query: String): Boolean {
	var queryChecking: String = query;
	while ("\\([^\\(]+\\)".toRegex().containsMatchIn(queryChecking)) {
		val parenthesised = "\\([^\\(]+\\)".toRegex().find(queryChecking)!!.value;
		val parenthesisedSliced = parenthesised.slice(1..parenthesised.length - 2);
		queryChecking = queryChecking.replace(parenthesised, parenthesisedSliced);
	}
	return !"\\(".toRegex().containsMatchIn(queryChecking)
		&& !"([^!+|^(]\\(|\\([^!A-Z(]|[^A-Z)]\\)|\\)[^+|^=)])".toRegex().containsMatchIn(query)
		&& "!*[A-Z]([+|^]!*[A-Z])*=>[A-Z](\\+[A-Z])*".toRegex().matches(queryChecking);
}

fun parseFacts(infos: Infos, query: String, line: String): Infos {
	val	facts: MutableList<String> = mutableListOf<String>();
	"[A-Z]".toRegex().findAll(query).forEach{
		facts.add(it.groupValues[0]);
		infos.unrecoverable = infos.unrecoverable.replace(it.groupValues[0], "");
	};
	if (facts.size != facts.distinct().size && ("[.=][A-Z]*".toRegex().matches(query)
			&& facts.filter{ infos.facts.areTrue.contains(it)
			|| infos.facts.areFalse.contains(it) }.size > 0)
			|| ("\\?[A-Z]*".toRegex().matches(query)
			&& facts.filter{ infos.facts.areSought.contains(it) }.size > 0)) {
		println("uncorrect line : ${line}");
		infos.error = true;
	}
	else if ("=[A-Z]*".toRegex().matches(query)) infos.facts.areTrue.addAll(facts);
	else if ("\\.[A-Z]*".toRegex().matches(query)) infos.facts.areFalse.addAll(facts)
	else if ("\\?[A-Z]*".toRegex().matches(query)) infos.facts.areSought.addAll(facts)
	else {
		println("uncorrect line : ${line}");
		infos.error = true;
	}
	return infos;
}

fun	parseFile(infos: Infos, filename: String): Infos {
	val file: File = File(filename);
	if (!file.exists()) {
		println("file not found");
		return infos;
	}
	var newInfos: Infos = infos;
	file.forEachLine { line ->
		val query: String = "(#.*|\\s)".toRegex().replace(line, "");
		if (!newInfos.error && query.length != 0) {
			if (checkQuery(query)) {
				"[A-Z]".toRegex().findAll(query.split("=>")[1]).forEach{
					newInfos.unrecoverable = newInfos.unrecoverable.replace(it.groupValues[0], "");
				}
				newInfos.rules.add(Rule(query));
			}
			else newInfos = parseFacts(newInfos, query, line);
		}
	}
	return newInfos;
}