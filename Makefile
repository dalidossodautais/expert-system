# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: ddosso-d <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/04/08 14:40:34 by ddosso-d          #+#    #+#              #
#    Updated: 2019/04/08 14:40:36 by ddosso-d         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

.PHONY: all fclean name re

NAME=		expert-system.jar

CC=			kotlinc
FLAGS=		 -include-runtime

SRCSPATH=	srcs/
FILES=		main parse resolve subResolve
SRCS=		$(addprefix $(SRCSPATH), $(addsuffix .kt, $(FILES)))

all: $(NAME)

$(NAME): $(SRCS)
	$(CC) $(SRCS) $(FLAGS) -d $(NAME)

clean:
	rm -rf $(NAME)

fclean: clean
	rm -rf tests/asked/this tests/basics/this tests/stolen/this

re: fclean all

tests: basics asked

asked:
	java -jar expert-system.jar tests/asked/**/* > tests/asked/this
	diff tests/asked/expected tests/asked/this

basics:
	java -jar expert-system.jar tests/basics/**/* > tests/basics/this
	diff tests/basics/expected tests/basics/this

stolen:
	java -jar expert-system.jar tests/stolen/**/* > tests/stolen/this
	# diff tests/stolen/expected tests/stolen/this
