# Expert System

Installer JDK sur l'ordinateur

Lancer la commande `curl -s https://get.sdkman.io | bash`

Relancer le terminal

Lancer la commande `sdk install kotlin`

Lancer la commande `kotlinc expert-system.kt -include-runtime -d expert-system.jar`

Essayer les fichiers avec `java -jar expert-system.jar filename`
